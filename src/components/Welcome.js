import { useState, useEffect } from "react";

export default function Welcome(props){

    const [ counter, setCounter ] = useState(0);
    const [ semaforo, setSemaforo] = useState(false);
    const { message, name } = props;

    useEffect(() => {
        console.log(semaforo);
    }, [semaforo]);

    // cada vez que el state de semaforo cambia se ejecuta eso del useEffect

    const contar = () => {
        setCounter(counter+1);
        setSemaforo(!semaforo);
    }

    return (
        <div>
            <p>Hola desde Welcome {name}</p>
            <h2>Contador de React con Hooks</h2>
            <p>El semáforo está en color { semaforo ? "red" : "green" }</p>
            <h4> El número del contador es {counter}</h4>
            <button type="submit" onClick={contar}>
                Sumar contador
            </button>
        </div>
    )
}